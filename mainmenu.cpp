#include "mainmenu.h"


MainMenu::MainMenu(QWidget *parent) : QWidget(parent){
    menuHLayout =new QHBoxLayout();

    newGameButton = new QPushButton("Новая игра");
    CloseGameButton = new QPushButton("Выход");

    menuHLayout->addWidget(newGameButton);
    menuHLayout->addWidget(CloseGameButton);

    this->setLayout(menuHLayout);

   connect(newGameButton,&QPushButton::clicked,this, &MainMenu::newGameClick);
   connect(CloseGameButton,&QPushButton::clicked,this,&MainMenu::exiitClick);
}

MainMenu::~MainMenu(){
    delete newGameButton;
    delete CloseGameButton;
    delete menuHLayout;
}
