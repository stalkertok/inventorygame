#include "itemdrag.h"


ItemDrag::ItemDrag(QWidget *widget) : QDrag(widget){
}

void ItemDrag::setItem(QString type, int count, bool external)
{
    MimeDataItem* mimeDataItem = new MimeDataItem;
    mimeDataItem->setType(type,count,external);
    setMimeData(mimeDataItem);
}
