#-------------------------------------------------
#
# Project created by QtCreator 2017-09-29T19:43:44
#
#-------------------------------------------------

QT       += core gui sql multimedia network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Inventory
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    inventoryitem.cpp \
    dbconnection.cpp \
    mimedataitem.cpp \
    itemdrag.cpp \
    inventory.cpp \
    GameInventoryWidget.cpp \
    inventorydb.cpp \
    mainmenu.cpp \
    gamefield.cpp \
    widgetresourse.cpp

HEADERS += \
        mainwindow.h \
    inventoryitem.h \
    dbconnection.h \
    mimedataitem.h \
    itemdrag.h \
    inventory.h \
    GameInventoryWidget.h \
    inventorydb.h \
    mainmenu.h \
    gamefield.h \
    widgetresourse.h

FORMS +=

RESOURCES += \
    media.qrc
