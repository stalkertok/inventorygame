#ifndef INVENTORY_H
#define INVENTORY_H

#include <QString>
#include <QObject>
#include <QVector>
#include <QPair>
#include "inventoryitem.h"
#include "inventorydb.h"

/**
 * @brief InventaryMatrix - тип двумерного массива из пар предмета инвентаря и его количества в ячейке
 */
typedef QVector<QVector<InventoryItem*>> InventaryMatrix;

const int ROWS=3;
const int COLUMNS=3;

/**
 * @brief The Inventory class - Класс инвентарь,  двумерное поле с количеством ячеек rowCount*columnCount
 */
class Inventory{

public:
    Inventory(qint32 rowCount,qint32 columnCount);
    ~Inventory();
    /**
     * @brief item -возрашает пару из предмета инвентаря и его количество в ячейке с индексами [row,column]
     * @param row
     * @param column
     * @return
     */
    const InventoryItem *item(qint32 row,qint32 column)const;

public slots:
    /**
     * @brief addItem - добавляет новый предмет ( с типом type) в инвентарь с количеством count по индексам  row, column
     * @param row
     * @param column
     * @param type
     * @param count
     */
    void addItem(qint32 row, qint32 column, QString type, qint32 count);
    /**
     * @brief RemoveItem удаляет предмет из инвентаря с количеством count по индексам  row, column
     * @param row
     * @param column
     * @param count
     */
    void RemoveItem(qint32 row, qint32 column, qint32 count);
    /**
     * @brief RemoveItem - удаляет предмет целиком из инвентаря по индексам  row, column
     * @param row
     * @param column
     */
    void RemoveItem(qint32 row, qint32 column);
    /**
     * @brief clear - очищает инвентарь
     */
    void clear();

private:

    /**
     * @brief fromBD - очищает и заполняет инвентарь из базы данных
     */
    void fromBD ();
    /**
     * @brief clearMatrix - очишает только инвентарь, не синхронизируя с БД
     */
    void clearMatrix();
    /**
     * @brief RemoveItemMatrix -удаляет элемент инентаря с координатами row, column
     * @param row - строка
     * @param column - столбец
     */
    void RemoveItemMatrix(qint32 row, qint32 column);

    /**
     * @brief inventoryMatrix - двумерный массив из пар предмета инвентаря и его количества в ячейке
     */
    InventaryMatrix inventoryMatrix;
    /**
     * @brief inventoryDB - класс синхронизации данных с БД
     */
    InventoryDB inventoryDB;

};

#endif // INVENTORY_H
