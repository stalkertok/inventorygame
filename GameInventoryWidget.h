#ifndef GAMEINVENTORYWIDGET_H
#define GAMEINVENTORYWIDGET_H

#include <QTableWidget>
#include "inventory.h"
#include <QtMultimedia/QMediaPlayer>


/**
 * @brief The GameInventoryWidget class - Класс для отображения инвентаря, а также обработки различных событий.
 */
class GameInventoryWidget : public QTableWidget
{
    Q_OBJECT
public:
    /**
     * @brief GameInventoryWidget - Создает класс с количеством строк rows и колонок columns,
     * также внутри создается объект типа Inventory(row, column)
     * @param rows -количество строк
     * @param columns -количество сстолбцов
     * @param parent - класс родитель
     */
    explicit GameInventoryWidget(int rows, int columns, QWidget *parent = nullptr);
    virtual ~GameInventoryWidget();

public slots:
    /**
     * @brief updateCells - обновление положения предметов в инвентаре
     */
    void updateCells();

    void newGame();
    /**
     * @brief clear - очищаем весь инвентарь
     */
    void clear();

private slots:

protected:
    /**
         * @brief dragEnterEvent, dragMoveEvent, dropEvent, mousePressEvent,  mouseMoveEvent - обрабатывают события dragAndDrop
         * @param event
         */
    virtual void dragEnterEvent(QDragEnterEvent* event);
    virtual void dragMoveEvent(QDragMoveEvent* event);
    virtual void dropEvent(QDropEvent* event);
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void mouseMoveEvent(QMouseEvent *event);
private:
    QPoint posPress;
    /**
     * @brief inventory - класс хранит в себе предметы типа InventoryItem
     */
    Inventory* inventory;
    QMediaPlayer* player;

};

#endif // GAMEINVENTORYWIDGET_H
