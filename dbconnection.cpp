#include "dbconnection.h"

DBConnection::DBConnection(const QString &dbName):_dbName(dbName){
    if(QSqlDatabase::contains(QSqlDatabase::defaultConnection)) {
        db = QSqlDatabase::database();
    } else {
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(dbName);
    }

    db.open();
}

DBConnection::~DBConnection(){
    this->close();
    QSqlDatabase::removeDatabase(_dbName);
}

bool DBConnection::open(){
    return db.open();
}

bool DBConnection::isOpen()const{
    return db.isOpen();
}

void DBConnection::close(){
    db.close();
}

QSqlQuery DBConnection::exec(const QString &query){
    return db.exec(query);
}

QSqlQuery DBConnection::queryExec(QSqlQuery query){
    query.exec();
    return query;
}

QSqlError DBConnection::lastError() const{
    return db.lastError();
}
