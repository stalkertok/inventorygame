#ifndef ITEMDRAG_H
#define ITEMDRAG_H

#include <QDrag>
#include <QWidget>
#include "mimedataitem.h"
/**
 * @brief The ItemDrag class - класс события Drag для предмета инвентаря
 */
class ItemDrag : public QDrag {
public:
    ItemDrag(QWidget* widget = 0);

    void setItem(QString type, int count,bool external);
};

#endif // ITEMDRAG_H
