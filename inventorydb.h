#ifndef INVENTORYDB_H
#define INVENTORYDB_H


#include <QSqlQuery>
#include <QSqlError>
#include "inventoryitem.h"

#include "dbconnection.h"

const QString dbInventory = "inventory.db";

class InventoryDB
{
public:
    InventoryDB();
    ~InventoryDB();

    /**
     * @brief insertToDB,removeToDB - отображает предметы инвентаря в БД
     */
    void insertItemDB(int rowItem, int columnItem, QString type, int Count);
    void removeItemDB(int rowItem, int columnItem);
    /**
     * @brief getItemInfo - получает информацию о предмете по его типу
     * @param type - тип предмета
     * @return - возращает  QSQLQUery результата запроса
     */
    QSqlQuery getItemInfo (const QString& type);
    /**
     * @brief fromDB- отображает предметы БД в инвентарь
     */
   QSqlQuery fromDB();

private:
    QSqlError InitInventoryDB();

    DBConnection db;
};

#endif // INVENTORYDB_H
