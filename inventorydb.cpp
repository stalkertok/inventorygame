#include "inventorydb.h"

#include <QVariant>
#include <QDebug>

InventoryDB::InventoryDB():db(DBConnection(dbInventory)){

    InitInventoryDB();

}

InventoryDB::~InventoryDB(){
}

/**
 * @brief InitInventoryDB - создает БД и заполняет данными
 * @return - ошибка выполнения sql запросов
 */
QSqlError InventoryDB::InitInventoryDB(){

    QString createTableInventoryItem = "CREATE TABLE InventoryItem (Type VARCHAR (100) PRIMARY KEY UNIQUE NOT NULL, ImagePath VARCHAR (150), SoundPath VARCHAR (150));";
    QString createTableInventory = "CREATE TABLE Inventory (rowItem INTEGER NOT NULL, columnItem INTEGER NOT NULL, InventoryItemID VARCHAR (100) REFERENCES InventoryItem (Type), Count INTEGER, PRIMARY KEY (rowItem,columnItem)ON CONFLICT IGNORE);";
    QString insertApple = "INSERT INTO InventoryItem (Type,ImagePath,SoundPath) VALUES ('apple',':img/apple.jpg','qrc:/sound/apple.mp3');";
    db.exec(createTableInventoryItem);
    db.exec(createTableInventory);
    db.exec(insertApple);

    return db.lastError();
}


void InventoryDB::insertItemDB(int rowItem, int columnItem, QString type, int Count){

    QSqlQuery insert;

    insert.prepare("INSERT OR REPLACE INTO Inventory (rowItem,columnItem,InventoryItemID,Count)VALUES(:rowItem,:columnItem,:InventoryItemID,:Count)");

    insert.bindValue(":rowItem",rowItem);
    insert.bindValue(":columnItem",columnItem);
    insert.bindValue(":InventoryItemID",type);
    insert.bindValue(":Count",Count);

    db.queryExec(insert);

}

void InventoryDB::removeItemDB(int rowItem, int columnItem){
    QSqlQuery deleteQuery;

    deleteQuery.prepare("DELETE FROM Inventory WHERE rowItem =:rowItem and columnItem =:columnItem ");

    deleteQuery.bindValue(":rowItem",rowItem);
    deleteQuery.bindValue(":columnItem",columnItem);

    db.queryExec(deleteQuery);

}

QSqlQuery InventoryDB::getItemInfo(const QString &type){
    QSqlQuery query;
    query.prepare("SELECT ImagePath,SoundPath FROM InventoryItem where type=:type");
    query.bindValue(":type",type);
    return db.queryExec(query);
}

QSqlQuery InventoryDB::fromDB(){

    return db.exec("SELECT rowItem,columnItem, InventoryItemID, Count,ImagePath,SoundPath FROM Inventory join InventoryItem on InventoryItemId=Type");
}
