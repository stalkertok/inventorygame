#include "widgetresourse.h"
#include <QSqlQuery>
#include <QVariant>
#include "itemdrag.h"

WidgetResourse::WidgetResourse(const QString &type,QWidget *parent):QLabel(parent){

    auto query=inventoryDB.getItemInfo(type);
    if (query.next()){
        item.setType(type);
        item.setImagePath(query.value(0).toString());
        item.setSoundPath(query.value(1).toString());

        this->setPixmap(QPixmap(item.imagePath()));
    }

}

InventoryItem WidgetResourse::Item(){
return item;
}

void WidgetResourse::mousePressEvent(QMouseEvent *event){

    if (event->button() == Qt::LeftButton) {

        auto drag = new ItemDrag(this);
        auto mimeData = new MimeDataItem;

        mimeData->setType(this->Item().type(),1,true);

        drag->setMimeData(mimeData);
        drag->setPixmap(*this->pixmap());
        drag->exec();

    }

}
