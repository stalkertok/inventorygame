#include "mimedataitem.h"

bool MimeDataItem::external() const
{
    return _external;
}

MimeDataItem::MimeDataItem() : QMimeData(){
}

MimeDataItem::~MimeDataItem(){
}

QString MimeDataItem::mimeType(){
    return "application/item";
}

void MimeDataItem::setType(QString type, int count, bool external){
    _type = type;
    _count = count;
    _external=external;
    setData(mimeType(), QByteArray());
}

QString MimeDataItem::type() const{
    return _type;
}

qint32 MimeDataItem::count() const{
    return _count;
}
