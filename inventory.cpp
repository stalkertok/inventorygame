#include "inventory.h"
#include <QDebug>
#include <dbconnection.h>
#include "inventoryitem.h"
#include <QSqlRecord>

Inventory::Inventory(qint32 rowCount, qint32 columnCount):inventoryDB(InventoryDB()){

    inventoryMatrix.resize(rowCount);

    for (auto i=0;i<rowCount;i++){
        inventoryMatrix[i].resize(columnCount);

    }

    fromBD();
}

Inventory::~Inventory(){
    for (auto i=0;i<inventoryMatrix.count();i++){
        for (auto j=0;j<inventoryMatrix[0].count();j++)
            delete inventoryMatrix[i][j];
        inventoryMatrix[i].clear();
    }
    inventoryMatrix.clear();
}

void Inventory::addItem(qint32 row, qint32 column, QString type, qint32 count){
    if (row>=inventoryMatrix.count() || column>=inventoryMatrix[0].count())
        return;

    if (inventoryMatrix[row][column]==nullptr){
        inventoryMatrix[row][column] =new InventoryItem(type);
        auto query=inventoryDB.getItemInfo(type);
        if (query.next()){
            inventoryMatrix[row][column]->setImagePath(query.value(0).toString());
            inventoryMatrix[row][column]->setSoundPath(query.value(1).toString());
        }
        inventoryMatrix[row][column]->setCount(count);
    }
    else
        if (inventoryMatrix[row][column]->type()==type)
            inventoryMatrix[row][column]->setCount(count);
    inventoryDB.insertItemDB(row,column,inventoryMatrix[row][column]->type(),inventoryMatrix[row][column]->count());
}

void Inventory::RemoveItem(qint32 row, qint32 column, qint32 count){
    if (row>=inventoryMatrix.count() || column>=inventoryMatrix[0].count())
        return;

    inventoryMatrix[row][column]->setCount(count);

    if (inventoryMatrix[row][column]->count()==0){
        inventoryDB.removeItemDB(row,column);
    }
    else
        inventoryDB.insertItemDB(row,column,inventoryMatrix[row][column]->type(),inventoryMatrix[row][column]->count());


}

void Inventory::RemoveItem(qint32 row, qint32 column){
    RemoveItemMatrix(row,column);
    inventoryDB.removeItemDB(row,column);
}

void Inventory::clear(){
    for (auto i=0;i<inventoryMatrix.count();i++)
        for (auto j=0;j<inventoryMatrix[0].count();j++)
            RemoveItem(i,j);

}

void Inventory::fromBD(){
    clearMatrix();

    QSqlQuery query = inventoryDB.fromDB();
    auto row =0;
    auto column =0;
    auto count = 0;
    QString imagePath;
    QString soundPath;
    QString type;

    while (query.next()) {
        row = query.value(0).toInt();
        column = query.value(1).toInt();
        type = query.value(2).toString();
        count = query.value(3).toInt();
        imagePath = query.value(4).toString();
        soundPath = query.value(5).toString();

        if (inventoryMatrix.count()>row  && inventoryMatrix[0].count()>column){

            inventoryMatrix[row][column] = new InventoryItem(type,imagePath,soundPath,count);
        }

    }
}

void Inventory::clearMatrix(){
    for (auto i=0;i<inventoryMatrix.count();i++)
        for (auto j=0;j<inventoryMatrix[0].count();j++)
            RemoveItemMatrix(i,j);
}

void Inventory::RemoveItemMatrix(qint32 row, qint32 column){
    if (row>=inventoryMatrix.count() || column>=inventoryMatrix[0].count())
        return;

    if (inventoryMatrix[row][column]!=nullptr)
        delete inventoryMatrix[row][column];
    inventoryMatrix[row][column]=nullptr;

}

const InventoryItem * Inventory::item(qint32 row, qint32 column)const{
    if (row<inventoryMatrix.count() && column<inventoryMatrix[0].count())
        return inventoryMatrix[row][column];
    else
        return nullptr;
}
