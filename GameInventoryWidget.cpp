#include <QDropEvent>
#include <QMimeData>
#include <QMessageBox>
#include <QDataStream>
#include <QApplication>
#include <appleitem.h>
#include <QHeaderView>

#include "GameInventoryWidget.h"
#include "mimedataitem.h"
#include "itemdrag.h"


GameInventoryWidget::GameInventoryWidget(int rows,int columns,QWidget *parent) :
    QTableWidget(rows,columns,parent),inventory(new Inventory(rows,columns)),player(new QMediaPlayer){

    this->setAcceptDrops(true);
    this->horizontalHeader()->setVisible(false);
    this->verticalHeader()->setVisible(false);

    this->horizontalHeader()->setDefaultSectionSize(150);
    this->verticalHeader()->setDefaultSectionSize(150);

    this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);

    // чтобы подпись была в левом нижнем углу
    for (auto row=0;row<this->rowCount();row++)
        for (auto column=0;column<this->columnCount();column++){
            auto item=new QTableWidgetItem();
            item->setTextAlignment(Qt::AlignBottom|Qt::AlignRight);
            this->setItem(row,column,item);
        }
}

GameInventoryWidget::~GameInventoryWidget(){    
    delete inventory;
    delete player;
}

void GameInventoryWidget::updateCells(){
    for (auto  i=0;i<this->rowCount();i++)
        for (auto  j=0;j<this->columnCount();j++){
            if (inventory->item(i,j)!=nullptr && inventory->item(i,j)->count()!=0){
                this->item(i,j)->setText(QString::number(inventory->item(i,j)->count()));
                this->item(i,j)->setBackground(QBrush(QPixmap(inventory->item(i,j)->imagePath())));
            }
            else{
                this->item(i,j)->setText("");
                this->item(i,j)->setBackground(QBrush());
            }
        }
}

void GameInventoryWidget::newGame(){

    inventory->clear();

    this->updateCells();
}

void GameInventoryWidget::clear(){
    inventory->clear();
    updateCells();

}

void GameInventoryWidget::dragEnterEvent(QDragEnterEvent *event){
    if (event->mimeData()->hasFormat(MimeDataItem::mimeType()))
        event->acceptProposedAction();
}

void GameInventoryWidget::dragMoveEvent(QDragMoveEvent *event){
    if (event->mimeData()->hasFormat(MimeDataItem::mimeType()))
        event->acceptProposedAction();
}

void GameInventoryWidget::dropEvent(QDropEvent *event){

    const MimeDataItem* mimedata=dynamic_cast<const MimeDataItem*>(event->mimeData());

    if (mimedata==nullptr){
        qDebug()<<"mime data id null";
        return;
    }


    auto row=rowAt(event->pos().y());
    auto column=columnAt(event->pos().x());
    auto rowPress=rowAt(posPress.y());
    auto columnPress=columnAt(posPress.x());

    if (row==rowPress && column==columnPress && !mimedata->external())
        return;

    if (row>=0 && column>=0 && row<rowCount() && column<columnCount()){

        //если событие от внешнего источника, например виджета ресурсов, то просто добавляем новый предмет
        if (mimedata->external()){
            if (inventory->item(row,column)==nullptr)
                inventory->addItem(row,column,mimedata->type(),mimedata->count());
            else{
                auto curCount =inventory->item(row,column)->count();
                inventory->addItem(row,column,mimedata->type(),mimedata->count()+curCount);
            }
        }
        else
            //Иначе будем перемещать предмет
            if (rowPress>=0 && columnPress>=0 && rowPress<rowCount() && columnPress<columnCount()){
                auto curCount =inventory->item(row,column)->count();
                inventory->RemoveItem(rowPress,columnPress,0);
                inventory->addItem(row,column,mimedata->type(),mimedata->count()+curCount);
            }
    }

    posPress.setY(0);
    posPress.setX(0);

    this->updateCells();
    event->acceptProposedAction();
}

void GameInventoryWidget::mousePressEvent(QMouseEvent *event){

    auto row=rowAt(event->pos().y());
    auto column=columnAt(event->pos().x());

    if (event->button() == Qt::LeftButton){
        posPress=event->pos();
    }

    if (event->button() == Qt::RightButton){

        if (row>=0 && column>=0 && row<rowCount() && column<columnCount() ){

            auto inventoryItem =inventory->item(row,column);

            if (inventoryItem!=nullptr){
                QString type =inventoryItem->type();
                player->setMedia(QMediaContent(QUrl(inventoryItem->soundPath())));
                player->play();
                inventory->RemoveItem(row,column,inventoryItem->count()-1);
                this->updateCells();
            }
        }
    }
}

void GameInventoryWidget::mouseMoveEvent(QMouseEvent *event){

    if (!(event->buttons() & Qt::LeftButton))
        return;

    if ((event->pos() - posPress).manhattanLength()< QApplication::startDragDistance())
        return;

    auto row=rowAt(event->pos().y());
    auto column=columnAt(event->pos().x());
    auto rowPress=rowAt(posPress.y());
    auto columnPress=columnAt(posPress.x());

    if (row>=0 && column>=0 && row<rowCount() && column<columnCount() &&
            rowPress>=0 && columnPress>=0 && rowPress<rowCount() && columnPress<columnCount()){

        if (inventory->item(rowPress,columnPress)!=nullptr)
            if  (!inventory->item(rowPress,columnPress)->type().isEmpty()){

                auto drag = new ItemDrag(this);
                auto mimeData = new MimeDataItem;

                auto inventoryItem=inventory->item(row,column);

                mimeData->setType(inventoryItem->type(),inventoryItem->count(),false);

                drag->setMimeData(mimeData);
                drag->setPixmap(QPixmap(inventoryItem->imagePath()));
                drag->exec();
            }
    }
}
