#include "mainwindow.h"
#include <QLayout>
#include <QPushButton>
#include <mainmenu.h>
#include "gamefield.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{

    this->resize(800,600);

    auto widget = new QWidget(this);

    auto vlayout = new QVBoxLayout();
    auto mainMenuButton = new QPushButton("Главное меню",this);

    GameField * gameField= new GameField(this);

    MainMenu *mainMenu=new MainMenu(this);
    mainMenuButton->setVisible(false);

    vlayout->addWidget(mainMenu);
    vlayout->addWidget(mainMenuButton);
    vlayout->addWidget(gameField);

    widget->setLayout(vlayout);
    this->setCentralWidget(widget);

    connect(mainMenu,&MainMenu::exiitClick,this,&MainWindow::close);

    connect(mainMenu,&MainMenu::newGameClick,[=](){
        gameField->newGame();
        mainMenu->setVisible(false);
        gameField->setEnabled(true);
        mainMenuButton->setVisible(true);
    });

    connect(mainMenuButton,&QPushButton::clicked,this,[=]{
        mainMenu->setVisible(true);
        gameField->setEnabled(false);
        mainMenuButton->setVisible(false);
    });


}

MainWindow::~MainWindow(){
}


