#ifndef GAMEFIELD_H
#define GAMEFIELD_H

#include <QWidget>
#include <QTableWidget>
#include "GameInventoryWidget.h"
#include "widgetresourse.h"

/**
 * @brief The GameField class - виджет в ключает в себя два виджета -виджет инвентаря и веджет ресурса, имеет слот для начала игрового процесса
 */

class GameField : public QWidget
{
    Q_OBJECT
public:
    explicit GameField(QWidget *parent = nullptr);
    ~GameField();

signals:

public slots:
    void newGame();

private:
    /**
     * @brief table - виджет для отображения и обработки действий с инвентарем
     */
    GameInventoryWidget* table;

    /**
     * @brief appleItem - класс ресурса (для генерации)
     */
    WidgetResourse* inventoryItem;
};

#endif // GAMEFIELD_H
