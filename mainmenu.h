#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include <QPushButton>
#include <QLayout>

/**
 * @brief The MainMenu class Виджет главного меню, Имеет два сигнала: начало игры, окончание игры
 */
class MainMenu : public QWidget
{
    Q_OBJECT
public:
    explicit MainMenu(QWidget *parent = nullptr);
    ~MainMenu();

signals:
    void newGameClick();
    void exiitClick();
public slots:

private:
    QPushButton *newGameButton;
    QPushButton *CloseGameButton;
    QHBoxLayout *menuHLayout;
};

#endif // MAINMENU_H
