#ifndef WIDGETRESOURSE_H
#define WIDGETRESOURSE_H

#include "inventorydb.h"
#include "inventoryitem.h"
#include <QLabel>
#include <QMouseEvent>

/**
 * @brief The WidgetResourse class -виджет ресурс предметов
 */
class WidgetResourse: public QLabel {
public:
    WidgetResourse(const QString& type, QWidget *parent=nullptr);
    /**
     * @brief Item - получаем елемент инвентаря
     * @return
     */
    InventoryItem Item();

private:
    InventoryDB inventoryDB;
    InventoryItem item;

protected:
    virtual void mousePressEvent(QMouseEvent* event);


};

#endif // WIDGETRESOURSE_H
