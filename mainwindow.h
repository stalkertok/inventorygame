#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


/**
 * @brief The MainWindow class главное окно программы
 */
class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:


};

#endif // MAINWINDOW_H
