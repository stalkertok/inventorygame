//#ifndef APPLEITEM_H
//#define APPLEITEM_H

//#include <QString>
//#include "inventoryitem.h"

//const QString APPLE ="apple";

///**
// * @brief The AppleItem class - класс для хранения предмета яблока
// */
//class AppleItem : public InventoryItem
//{
//public:
//    AppleItem();
//    virtual ~AppleItem();
//    /**
//     * @brief imagePathUpdate - Метод возращает значение imagePath из БД, а также сохраняет его в _imagePath
//     * @return - _imagePath
//     */
//    QString imagePathUpdate();

//    /**
//     * @brief soundPathUpdate - Метод возращает значение soundPath из БД, а также сохраняет его в _soundPath
//     * @return значение _soundPathUpdate
//     */
//    QString soundPathUpdate();

//    /**
//     * @brief soundPath - Метод возращает значение _soundPath
//     * @return - значение _soundPath
//     */
//    QString soundPath() const;

//    /**
//     * @brief action - перегруженный метоб базового класс, Воспроизводит звук откусывания яблока (:qrc/sound/apple.mp3)
//     */
//    virtual void action()override;

//private:
//    QString _soundPath;
//};

//#endif // APPLEITEM_H
