#ifndef MIMEDATAITEM_H
#define MIMEDATAITEM_H

#include <QDrag>
#include "inventoryitem.h"
#include <QMimeData>

/**
 * @brief The MimeDataItem class -класс для передачи данных о переносимом предмете при DragAndDrop
 */
class MimeDataItem : public QMimeData {
private:
    /**
     * @brief _type - тип предмета
     */
    QString _type;
    /**
     * @brief _count - количество предметов
     */
    int _count;
    /**
     * @brief _external - событие пришло от виджжета инвентаря (_external=false) или от других виджетов
     */
    bool _external;

public:
    MimeDataItem();

    virtual ~MimeDataItem();

    static QString mimeType();

    /**
     * @brief setType - заполняет данные для события DragAndDrop
     * @param type
     * @param count
     * @param external
     */
    void setType(QString type, int count, bool external);

    /**
     * @brief type, count, external - получаем значения полей класса
     * @return
     */
    QString type() const;
    int count() const;
    bool external() const;
};

#endif // MIMEDATAITEM_H
