#include "gamefield.h"
#include <QLayout>

GameField::GameField(QWidget *parent) : QWidget(parent){

    table = new GameInventoryWidget(ROWS,COLUMNS);
    inventoryItem = new WidgetResourse(APPLE);
    inventoryItem->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);

    auto gameFieldhLayout =new QGridLayout();

    this->setDisabled(true);
    this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    this->setLayout(gameFieldhLayout);

    gameFieldhLayout->addWidget(table,0,0,3,1);
    gameFieldhLayout->addWidget(inventoryItem,0,1);

    table->updateCells();
}

GameField::~GameField(){
    delete inventoryItem;
    delete table;

}

void GameField::newGame(){
    table->newGame();
}
