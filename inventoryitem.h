#ifndef INVENTORYITEM_H
#define INVENTORYITEM_H

#include <QString>

/**
 * @brief The InventoryItem class - предмет инвентаря имеет тип предмета(строка), путь к изображению предмета
 */

const QString APPLE ="apple";

class InventoryItem{

public:
    InventoryItem();
    ~InventoryItem();
    InventoryItem(const QString &type,const QString &imagePath,QString &soundPath,qint32 count);
    InventoryItem(const QString &type);

    QString type() const;
    void setType(const QString &type);

    QString imagePath() const;
    void setImagePath(const QString &imagePath);

    QString soundPath() const;
    void setSoundPath(const QString &soundPath);

    qint32 count() const;
    void setCount(const qint32 &count);

private:
    QString _type;
    QString _imagePath;
    QString _soundPath;
    qint32 _count;
};

#endif // ITEMINVENTORY_H
