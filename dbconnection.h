#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QString>

/**
 * @brief The DBConnection class - Класс для выполнения sql запросов, создания соединения с БД. По умолчанию создается БД dbInventory
 */
class DBConnection{

public:

    DBConnection(const QString& dbName);
    ~DBConnection();

    bool open();
    bool isOpen()const;
    void close();
    /**
    * @brief exec - Выполняет запрос
    * @param queryString -строка запроса
    * @return - резкльтат выполнения запроса
    */
    QSqlQuery exec(const QString& queryString);
    /**
     * @brief queryExec - Выполняет запрос
     * @param query - запрос в виде QSQLQuery
     * @return -результат выполнения запроса
     */
    QSqlQuery queryExec(QSqlQuery query);
    QSqlError lastError() const;

private:
    QSqlDatabase db;
    QString _dbName;
};

#endif // DBCONNECTION_H
