#include "inventoryitem.h"
#include <QObject>

#include "dbconnection.h"

InventoryItem::InventoryItem():_type(QString()),_imagePath(QString()),_soundPath(QString()){

}

InventoryItem::~InventoryItem(){

}

InventoryItem::InventoryItem(const QString &type, const QString &imagePath, QString &soundPath, qint32 count):_type(type),_imagePath(imagePath),_soundPath(soundPath),_count(count){

}

InventoryItem::InventoryItem(const QString &type):_type(type){

}

QString InventoryItem::type() const{
    return _type;
}

void InventoryItem::setType(const QString &type){
    _type = type;
}

QString InventoryItem::imagePath() const{
    return _imagePath;
}

void InventoryItem::setImagePath(const QString &imagePath){
    _imagePath = imagePath;
}

QString InventoryItem::soundPath() const
{
    return _soundPath;
}

void InventoryItem::setSoundPath(const QString &soundPath){
    _soundPath = soundPath;
}

qint32 InventoryItem::count() const
{
    return _count;
}

void InventoryItem::setCount(const qint32 &count)
{
    _count = count;
}


